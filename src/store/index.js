import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: { Acelerometro: {} },
  mutations: {
    update_acelerometro(state, Acelerometro) {
      this.state.Acelerometro = Acelerometro;
    }
  },
  actions: {
    update_acelerometro({ commit }, Acelerometro) {
      commit("update_acelerometro", Acelerometro);
    }
  },
  modules: {},
  getters: {
    getAcelerometro: state => state.Acelerometro
  }
});
