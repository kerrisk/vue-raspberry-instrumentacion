import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueKonva from "vue-konva";
import JaysonRPC from "jayson-rpc-vue";
Vue.use(JaysonRPC, process.env.VUE_APP_RASPBERRY_ENDPOINT, false);
Vue.config.productionTip = false;
Vue.use(VueKonva);
new Vue({
  router,
  store,
  VueKonva,
  vuetify,
  render: h => h(App)
}).$mount("#app");
